/*
 * BufferManager.hpp
 *
 *  Created on: 19.04.2014
 *      Author: Daniel Hugenroth, Moritz Wilfer
 */

#ifndef BUFFERMANAGER_HPP_
#define BUFFERMANAGER_HPP_

#define PAGE_SIZE 16384 // 16 KB
#define PAGE_FILES "pages/"
#define RETRIES 10

#include <cstdlib>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <list>
#include <unordered_map>
#include <vector>
#include <string>
#include <sstream>
#include <exception>
#include <atomic>
#include <mutex>
#include "BufferFrame.hpp"

// some custom exception
class unable_to_free : public std::exception {
public:
  unable_to_free() { }
  ~unable_to_free() throw() { }
  const char* what() const throw() { return "unable to free buffer frame for page"; }
};

/*struct mutex_wrapper {
   mutex_wrapper() { }
   mutex_wrapper(const mutex_wrapper &x) { }
   
   std::mutex m;
};*/


// BufferManager - keeps the page-tables in main memory
class BufferManager {
public:
   BufferManager(unsigned size) : size(size), curr_in_memory(0) { }
   ~BufferManager();

   BufferFrame& fixPage(uint64_t pageId, bool exclusive);
   void unfixPage(BufferFrame& frame, bool isDirty);

private:
   unsigned size;
   std::atomic<unsigned> curr_in_memory;

   std::unordered_map<segment_id_t, std::unordered_map<page_id_t, BufferFrame *> > pids;
   
   std::unordered_map<segment_id_t, std::vector<page_id_t> > pages_in_segment;
   std::unordered_map<segment_id_t, std::unordered_map<page_id_t, int32_t> > page_positions;
   std::unordered_map<segment_id_t, page_id_t> pages_written_out;
   std::unordered_map<segment_id_t, FILE *> page_tables;
   std::unordered_map<segment_id_t, FILE *> page_files;

   std::list<BufferFrame *> fifo;

   std::mutex single;
   //std::unordered_map<segment_id_t, mutex_wrapper> s_mutex;
   //std::unordered_map<segment_id_t, std::unordered_map<page_id_t, mutex_wrapper> > p_mutex;

   void write_back_page_table(const segment_id_t segment_id);
   void write_back(BufferFrame *bf);
   uint32_t get_page_position(const segment_id_t segment_id, const page_id_t page_id);
   void try_to_free_page();
   void open_segment_file(const segment_id_t segment_id);

   std::string get_pt_filename(segment_id_t segment_id) {
      std::stringstream s;
      s << PAGE_FILES << "/" << segment_id << "_pt";
      return s.str();
   }
   std::string get_filename(segment_id_t segment_id) {
      std::stringstream s;
      s << PAGE_FILES << "/" << segment_id;
      return s.str();
   }
   std::string get_temp_filename(segment_id_t segment_id) {
      std::stringstream s;
      s << PAGE_FILES << "/" << segment_id << "_temp";
      return s.str();
   }
};

#endif /* BUFFERMANAGER_HPP_ */
