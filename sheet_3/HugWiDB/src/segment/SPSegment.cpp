/*
 * SPSegment.cpp
 *
 *  Created on: 05.05.2014
 *      Author: Daniel Hugenroth, Moritz Wilfer
 */

#include "SPSegment.hpp"

// Slotted Page implementation
//--------------------------------------
SlottedPage::Slot &SlottedPage::getSlot(const record_id_t &r_id) {
   return slots[r_id];  
}

TID SlottedPage::getTID(const uint16_t &offset) {
   TID tid;
   memcpy(&tid, (char *)&lsn + offset, sizeof(TID));
   return tid;
}

Record SlottedPage::getRecord(const uint16_t &length, const uint16_t &offset) {
   // copies the data in the record
   Record r(length, (char *)&lsn + offset);
   return r;   
}

record_id_t SlottedPage::insertRecord(const Record &r, bool indirected) {
   // copies the record in the page (we know that there is enough space!)
   dataStart -= r.getLen();
   char *address = (char *)&lsn + dataStart;
   memcpy(address, r.getData(), r.getLen());

   // set slot information
   slots[firstFreeSlot].fields.t = 255; // slot should not be interpreted as tid
   slots[firstFreeSlot].fields.s = indirected; // no indirection
   slots[firstFreeSlot].fields.offset = dataStart;
   slots[firstFreeSlot].fields.length = r.getLen();

   record_id_t r_id = firstFreeSlot;

   if (r_id + 1 > slotCount)
      slotCount++;

   // get next free slot
   while (1) {
      firstFreeSlot++;
      if (slots[firstFreeSlot].tid == 0 || firstFreeSlot == slotCount)
         break;
   }

   return r_id;
}

void SlottedPage::updateRecord(const record_id_t &r_id, const Record &r) {
   Slot &s = slots[r_id];

   // update data
   char *address = (char *)&lsn + s.fields.offset;
   memcpy(address, r.getData(), r.getLen());

   // update slot
   slots[r_id].fields.length = r.getLen();
}

bool SlottedPage::hasFreeSpace(page_id_t size, bool compressed) {
   // newly allocated pages have dataStart point to zero -> quick fix   
   if (dataStart == 0)
      dataStart = PAGE_SIZE;

   // check whether there is enough space on page
   int32_t free = dataStart - sizeof(LSN) - 2 * sizeof(record_id_t) - 2 * sizeof(uint64_t) - (slotCount + 1) * sizeof(Slot); 

   if (!compressed && free < (int32_t) size && (freeSpace - free) >= (int32_t) size) {
      compress();
      return hasFreeSpace(size, true);   
   }

   return free >= (int32_t) size;
}

// TODO: this method seems to be buggy :(
void SlottedPage::compress() {
   if (freeSpace == 0)
      return;
    
   /*// prepare new buffer page
   std::cout << "Compress -> free-space: " << freeSpace << std::endl;
   char *new_page = (char *) malloc(PAGE_SIZE);
   uint64_t start = PAGE_SIZE;

   std::cout << "SC: " << slotCount << std::endl;
   for (int32_t i = 0; i < slotCount; i++) {
      // check whether we have a record on the page
      if (slots[i].fields.t == 255) {
         std::cout << (int) slots[i].fields.s << "-" << slots[i].fields.length << "-> " << slots[i].fields.offset << "/" << slots[i].fields.length << std::endl;
         start -= slots[i].fields.length;
         memcpy(new_page + start, (char *) &lsn + slots[i].fields.offset, slots[i].fields.length);
         slots[i].fields.offset = start;
      }
   }

   // copy new page to real slotted page
   std::cout << "old-start: " << dataStart << "/new-start: " << start << std::endl;
   memcpy((char *) &lsn + start, new_page, PAGE_SIZE - start);
   dataStart = start;
   freeSpace = 0;*/
}

// Slotted Page SEGMENT implementation
//--------------------------------------
TID SPSegment::insert(const Record &r, bool indirected) {
   // generate pageId
   uint64_t pageId = ((uint64_t)riu->segment_id << (sizeof(page_id_t) * 8)) + riu->pages;
   BufferFrame &bf = bm->fixPage(pageId, true);
   SlottedPage& sp = *reinterpret_cast<SlottedPage *>(bf.getData());

   // check whether there is place to insert page here
   if (sp.hasFreeSpace(r.getLen())) {

      // insert on current slotted page
      record_id_t r_id = sp.insertRecord(r, indirected);
      bm->unfixPage(bf, true);

      TID tid = ((uint64_t)riu->pages << (sizeof(record_id_t) * 8)) + r_id;
      return tid;

   } else {

      // write to next page
      riu->incPages();
      bm->unfixPage(bf, true);
      return insert(r);

   }
}

bool SPSegment::remove(TID tid, bool indirected) {
   // generate pageId
   page_id_t page_id = tid / RECORDID_MAX;
   record_id_t record_id = tid;
   uint64_t pageId = ((uint64_t)riu->segment_id << (sizeof(page_id_t) * 8)) + page_id;

   BufferFrame &bf = bm->fixPage(pageId, true); // lookup is read-only
   SlottedPage& sp = *reinterpret_cast<SlottedPage *>(bf.getData());

   // get slot
   SlottedPage::Slot &slot = sp.getSlot(record_id);
   
   // check whether this is a free slot -> remove is not possible
   if (slot.tid == 0)
      return false;

   if (slot.fields.t != 255) {

      // indirection found
      TID i_tid = slot.tid;
      slot.tid = 0; // dirty dirty ;)
      if (record_id < sp.getFirstFreeSlot())
         sp.setFirstFreeSlot(record_id);
      bm->unfixPage(bf, true);
   
      // follow indirection
      return remove(i_tid);   

   } else {

      sp.incFreeSpaceBy(slot.fields.length);
      slot.tid = 0; // dirty dirty ;)
      if (record_id < sp.getFirstFreeSlot())
         sp.setFirstFreeSlot(record_id);
      bm->unfixPage(bf, true);
      return true;

   }
}

// assumption: lookup always happens with valid TIDs
Record SPSegment::lookup(TID tid, TID o_tid) {
   // generate pageId
   page_id_t page_id = tid / RECORDID_MAX;
   record_id_t record_id = tid;
   uint64_t pageId = ((uint64_t)riu->segment_id << (sizeof(page_id_t) * 8)) + page_id;

   BufferFrame &bf = bm->fixPage(pageId, false); // lookup is read-only
   SlottedPage& sp = *reinterpret_cast<SlottedPage *>(bf.getData());
   
   // get slot
   SlottedPage::Slot &slot = sp.getSlot(record_id);

   // check whether slot is free
   if (slot.tid == 0)
      throw free_slot();

   // check whether we got an indirection
   if (slot.fields.t != 255) {

      // redirection! -> interpret slot as tid
      bm->unfixPage(bf, false);
      return lookup(slot.tid, (o_tid == 0 ? tid : o_tid));

   } else {

      // read record
      if (slot.fields.s != 0) {

         // indirected record
         TID found_tid = sp.getTID(slot.fields.offset);
         if (found_tid != o_tid) 
            throw indirection_error(found_tid, o_tid);
         Record r(std::move(sp.getRecord(slot.fields.length - sizeof(TID), slot.fields.offset + sizeof(TID))));
         bm->unfixPage(bf, false);
         return r;      

      } else {

         // no indirection for this record
         Record r(std::move(sp.getRecord(slot.fields.length, slot.fields.offset)));
         bm->unfixPage(bf, false);
         return r; 

      }
   }
}

bool SPSegment::update(TID tid, const Record &r, bool indirected) {
   // generate pageId
   page_id_t page_id = tid / RECORDID_MAX;
   record_id_t record_id = tid;
   uint64_t pageId = ((uint64_t)riu->segment_id << (sizeof(page_id_t) * 8)) + page_id;

   BufferFrame &bf = bm->fixPage(pageId, true);
   SlottedPage& sp = *reinterpret_cast<SlottedPage *>(bf.getData());
   SlottedPage::Slot &s = sp.getSlot(record_id);

   // check whether this is a free slot -> no update possible
   if (s.tid == 0)
      return false;

   // follow indirection if there is one
   if (s.fields.t != 255) {
      bm->unfixPage(bf, true);  
      
      // check whether tid has to be added infront of record     
      if (!indirected) {
         size_t n_len = r.getLen() + sizeof(TID);
         char *buffer = (char *) malloc(n_len);
         memcpy(buffer, &tid, sizeof(TID));
         memcpy(buffer + sizeof(TID), r.getData(), r.getLen());
         Record r_new(n_len, buffer);
         return update(s.tid, r_new, true);
      } else 
         return update(s.tid, r, true);
   }

   // check whether update can be done in place
   if (s.fields.length >= r.getLen()) {

      // update in place (indirection flag has to survive!)
      sp.incFreeSpaceBy(s.fields.length - r.getLen());
      sp.updateRecord(record_id, r);
      bm->unfixPage(bf, true);
      return true;

   } else {
   
      // indirect update      
      sp.incFreeSpaceBy(s.fields.length);
      bm->unfixPage(bf, true);
      
      // check whether tid has to be added infront of record     
      TID i_tid;
      if (!indirected) {
         size_t n_len = r.getLen() + sizeof(TID);
         char *buffer = (char *) malloc(n_len);
         memcpy(buffer, &tid, sizeof(TID));
         memcpy(buffer + sizeof(TID), r.getData(), r.getLen());
         Record r_new(n_len, buffer);
         i_tid = insert(r_new, true);
      } else 
         i_tid = insert(r, true);
   
      // update slot      
      s.tid = i_tid;

      return true;
   }
}
