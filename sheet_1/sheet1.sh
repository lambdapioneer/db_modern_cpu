if [ "$#" -ne 2 ]; then
   echo "usage $0 <test_size_in_GB> <buffer_size_in_MB>"
   exit 1
fi

make clean
make

# generate test file
cd gen
make clean
make gen
./gen ../data $(($1 * 134217728)) # 134217728 = 1024 * 1024 * 1024 / 8 
cd ..

# run test
./test data output $2
