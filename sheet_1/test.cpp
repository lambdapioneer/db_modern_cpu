#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>

#include "fcntl.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdint>
#include <cassert>
#include <sstream>

using namespace std;

int main(int argc, char* argv[]){ 	
  if(argc != 1+3){
    fprintf(stderr, "Usage: %s <inputFile> <outputFile> <memoryBufferInMB>\n", argv[0]);
    exit(1);
  }
  
  std::stringstream s;
  s << "./sort \"" << argv[1] << "\" \"" << argv[2] << "\" " << argv[3];
  // sort
  if (!system(s.str().c_str())) { 
		// open files
		const int fdTest = open64(argv[2], O_RDONLY);
	 
		struct stat st;
  	fstat(fdTest, &st);
  	uint64_t size = st.st_size / sizeof(uint64_t); // 64 bit integers take up 8 byte

		uint64_t a, b;
		for (uint64_t i = 0; i < size; i++) {
			// read value
			if (read (fdTest, &a, sizeof(uint64_t)) == sizeof(uint64_t)) {		
				// check whether new integer is bigger than the older one
				if (i > 0)
					assert(a >= b);

				// store old value
				b = a;
			}
		}

		printf("all values are sorted!\n");

		// close
		close(fdTest);
  }
	
	return 0;
}
