#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cmath>
#include <algorithm>


#include "fcntl.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdint>

using namespace std;

/*
 * TODO: free all buffers
 */

#define TEMP_FILE_NAME "temp.tmp"

typedef struct{
    int idx_in_chunk; // at which subchunk of the i-th chunk
    int idx;          // at which element in the active subchunk of the i-th chunk
    int elements;     // how many elements in the active subchunk of the i-th chunk
    uint64_t* buffer; // content of the active subchunk of the i-th chunk
  } subchunk;

void externalSort(int fdInput, uint64_t size, int fdOutput, uint64_t memSize){
  printf("size=%ld, memSize=%ld\n", size, memSize);
  // create temp file
  const int fdTemp = open64(TEMP_FILE_NAME, O_CREAT|O_TRUNC|O_RDWR, S_IRUSR|S_IWUSR);

  // general purpose buffer
  uint64_t *buffer = (uint64_t *) malloc(memSize);

  // calculation of sizes and number of chunks
  uint64_t elements_per_chunk = min(size, memSize / sizeof(uint64_t));
  size_t size_per_chunk = elements_per_chunk * sizeof(uint64_t);
  const int64_t num_chunks = (size + elements_per_chunk - 1) / elements_per_chunk;
  

  // ---------------------------------------------
  // STEP 1: get chunks, sort chunks, write chunks
  vector<int> elements_in_chunk;
  
  for (size_t i = 0; i < num_chunks; i++) {
    // last chunk must not be full
    uint64_t num_elements = (i < num_chunks - 1 ? elements_per_chunk : size % elements_per_chunk);
    // special case: if file size is multiple of memory size
    if (num_elements == 0)
      num_elements = elements_per_chunk;
    
    // special case: if everything fits into memory, load everything (other wise the module will result null)
    if(num_chunks == 1)
      num_elements = size;
      
    size_t s = num_elements * sizeof(uint64_t);
    
    // remember how many elements are in this chunk
    elements_in_chunk.push_back(num_elements);
    
    // read chunk from file
    printf("Read chunk %ld with size %ld\n", i, s);
    if (read (fdInput, buffer, s) != s) {
       fprintf(stderr, "Problems reading chunk from input file\n");
       exit(1);
    }
    
    // sort chunk
    std::sort(buffer, buffer + num_elements);
    
    // write chunk to temp file
    if (write (fdTemp, buffer, s) != s) {
       fprintf(stderr, "Problems writing chunk to temp file\n");
       exit(1);
    }
  }
  free(buffer);
  
  // flush the temp file (TODO: check if necessary or even negative)
  fsync(fdTemp);

  // ----------------------------------------------------
  // STEP 2: merge the chunks and write to the final file
  
  // +1 for the output buffer  
  uint64_t elements_per_subchunk = memSize / (num_chunks + 1) / sizeof(uint64_t);
  size_t size_per_subchunk = elements_per_subchunk * sizeof(uint64_t);
  
  // data structures for the subchunks     
  vector<subchunk> subchunks;
  
  // output buffer
  uint64_t* output_buffer = (uint64_t*) malloc(size_per_subchunk);
  int output_buffer_idx = 0;
  
  // init subchunk datastructures
  for(int i=0; i<num_chunks; i++){
    subchunks.push_back(subchunk());
    
    subchunks[i].idx_in_chunk = -1;
    subchunks[i].idx = 0;
    subchunks[i].elements = 0;
    subchunks[i].buffer = (uint64_t*) malloc(size_per_subchunk);

   }
  
  while(true){    
    // check if we have to refresh one of the sub_chunks
    for(int i=0; i<num_chunks; i++){
      const int num_subchunks = elements_in_chunk[i] / elements_per_subchunk;
      
      // CASE A: already out of bounds
      if(subchunks[i].idx_in_chunk >= num_subchunks)
        continue;
        
      // CASE B: still within subchunk borders
      if(subchunks[i].idx < subchunks[i].elements)
        continue;
      
      // CASE C: we need to load a new subchunk   
      //printf("Load buffer %d\n", i);    
      const uint64_t offset_in_chunk = elements_per_subchunk * (subchunks[i].idx_in_chunk + 1);
      
      const uint64_t elems = min(elements_per_subchunk, elements_in_chunk[i] - offset_in_chunk );
      const size_t s = elems * sizeof(uint64_t);
      
      // do the actual read
      off_t offset = size_per_chunk * i + offset_in_chunk * sizeof(uint64_t);
      if (lseek(fdTemp, offset, SEEK_SET) != offset){
        fprintf(stderr, "Problems setting position %ld in temp file\n", offset);
        exit(1);
      }
      if (read (fdTemp, subchunks[i].buffer, s) != s) {
        fprintf(stderr, "Problems reading subchunk from temp file\n");
        exit(1);
      }
      
      subchunks[i].idx_in_chunk++;
      subchunks[i].idx = 0;
      subchunks[i].elements = elems;
    }
    
    // get the first elements of all active ones
    uint64_t min = 0xffffffffffffffff;
    int minIdx = -1;
    for(int i=0; i<num_chunks; i++){
      const int num_subchunks = subchunks[i].elements / elements_per_subchunk;    
      
      // totally finished this subchunk (so we ignore it)
      if(subchunks[i].idx >= subchunks[i].elements)
        continue;
    
      // get front most element
      const uint64_t x = subchunks[i].buffer[subchunks[i].idx];
      //printf("x=%lu\n", x);
      if(x < min){
        min = x;
        minIdx = i;
      }
    }
    
    // if nothing has been found, all chunks must have been finished
    if(minIdx == -1) break;
        
    // write to output_buffer
    output_buffer[output_buffer_idx] = min;
    output_buffer_idx++;
    
    // advance in subchunk
    subchunks[minIdx].idx++;
    
    // flush and free output_buffer if full
    if(output_buffer_idx == elements_per_subchunk){
      //printf("writing1!\n");
      // write chunk to output file
      size_t len = output_buffer_idx * sizeof(uint64_t);
      if (write (fdOutput, output_buffer, len) != len) {
         fprintf(stderr, "Problems writing output_puffer to output file\n");
         exit(1);
      }
      
      // reset output buffer pos
      output_buffer_idx = 0;
    }
  }
  
  // flush remaining output_buffer (if any)
  if(output_buffer_idx > 0){
    //printf("writing2!\n");
    size_t len = output_buffer_idx * sizeof(uint64_t);
    if (write (fdOutput, output_buffer, len) != len) {
           fprintf(stderr, "Problems writing output_puffer to output file\n");
           exit(1);
    }
  }
  fsync(fdOutput);
  
  // close and remove temp file
  close(fdTemp);
  remove(TEMP_FILE_NAME);
}


int main(int argc, char* argv[]){ 	
  if(argc != 1+3){
    fprintf(stderr, "Usage: %s <inputFile> <outputFile> <memoryBufferInMB>\n", argv[0]);
    exit(1);
  }
  
  // open files
  const int fdInput = open64(argv[1], O_RDONLY);
  const int fdOutput = open64(argv[2], O_CREAT|O_TRUNC|O_WRONLY, S_IRUSR|S_IWUSR);
  uint64_t memSize = atol(argv[3]) * 1024l * 1024l;
  
  struct stat st;
  fstat(fdInput, &st);
  uint64_t size = st.st_size / sizeof(uint64_t); // 64 bit integers take up 8 byte
  
  // run
  externalSort(fdInput, size, fdOutput, memSize);
  
  // close
  close(fdInput);
  close(fdOutput);
	
	return 0;
}
