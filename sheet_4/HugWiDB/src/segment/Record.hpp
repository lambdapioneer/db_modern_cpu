#ifndef H_Record_HPP_
#define H_Record_HPP_

#include <cstring>
#include <cstdlib>
#include <cstdint>

typedef uint32_t record_id_t;
#define RECORDID_MAX UINT32_MAX

// A simple Record implementation
class Record {
   unsigned len;
   char* data;

public:
   // Assignment Operator: deleted
   Record& operator=(Record& rhs) = delete;
   // Copy Constructor: deleted
   Record(Record& t) = delete;
   // Move Constructor
   Record(Record&& t);
   // Constructor
   Record(unsigned len, const char* const ptr);
   Record() { };
   // Destructor
   ~Record();
   // Get pointer to data
   const char* getData() const;
   // Get data size in bytes
   unsigned getLen() const;
};

#endif /* H_RECORD_HPP_ */
