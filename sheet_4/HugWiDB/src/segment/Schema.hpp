/*
 * Schema.hpp
 *
 *  Created on: 03.05.2014
 *      Author: Daniel Hugenroth, Moritz Wilfer
 */

#ifndef SCHEMA_HPP_
#define SCHEMA_HPP_

#include "../buffer/BufferFrame.hpp"
#include <vector>
#include <unordered_map>
#include <string>
#include <sstream>

#define SEPERATOR '|'

// Schema - segment that stores the schema of the database
class Schema {
public:
   // struct representing a type information unit
   struct TypeIU {
      enum Type { Char, Varchar, Tinytext, Text, Blob, Mediumtext, Mediumblob, Longtext, Longblob, Tinyint, Smallint, Mediumint, Int, Bigint, Float, Double, Decimal, Date, Datetime, Timestamp, Time, Year };   
      Type type;

      // special fields for some types
      uint32_t len;
      uint32_t dec;   
   };

   // struct representing an attribute information unit
   struct AttributeIU {
      TypeIU type;
      std::string name;
   };

   // struct representing a relation information unit
   struct RelationIU {
      segment_id_t segment_id;
      page_id_t pages;
      std::string name;
      std::vector<AttributeIU> attributes;

      void incPages() { pages++; }
   };
   
   // struct representing a index information unit
   struct IndexIU {
      segment_id_t segment_id;
      page_id_t pages;
      page_id_t root;
      std::string relation;
      std::string attribute;
      
      void incPages() { pages++; }
   };

   Schema() : counter(1) { }
   Schema(const std::string &buffer) : counter(1) {
      deserialize(buffer);
   }
   ~Schema() { }

   void addRelation(std::string name, std::vector<AttributeIU> aius) {
      relations.insert({name, {counter++, 0, name, aius}});   
   
   }
   RelationIU *getRelationIU(const std::string &name) {
      return &relations[name];
   }

   void deserialize(const std::string &buffer);
   std::string serialize() const;
private:
   uint32_t counter;
   std::unordered_map<std::string, RelationIU> relations;

   void _deserializeRelation(const std::string &buffer);
   void _deserializeAttribute(const std::string &buffer, RelationIU *riu);
   void _deserializeType(const std::string &buffer, AttributeIU *aiu);
};

std::ostream& operator<<(std::ostream& out, const Schema::RelationIU& r);
std::ostream& operator<<(std::ostream& out, const Schema::AttributeIU& r);
std::ostream& operator<<(std::ostream& out, const Schema::TypeIU& r);

#endif /* SCHEMA_HPP_ */
