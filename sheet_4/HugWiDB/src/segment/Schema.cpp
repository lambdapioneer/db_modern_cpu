/*
 * Schema.cpp
 *
 *  Created on: 03.05.2014
 *      Author: Daniel Hugenroth, Moritz Wilfer
 */

#include "Schema.hpp"

// serialization
std::string Schema::serialize() const {
   std::stringstream s;
   for (auto &r : relations) {
      s << "{";   
      s << r.second;
      s << "}";
   }
   return s.str();
}

// deserialization
void Schema::deserialize(const std::string &buffer) {
   size_t prev_pos = 1;
   size_t pos = 1;
   while ((pos = buffer.find_first_of("}", prev_pos)) != std::string::npos) {
      _deserializeRelation(buffer.substr(prev_pos, pos - prev_pos));
      pos++;
      prev_pos = pos + 1;
   }  
}

void Schema::_deserializeRelation(const std::string &buffer) {
   std::istringstream s(buffer);

   segment_id_t segment_id;
   s >> segment_id;

   char del;
   s >> del;

   page_id_t pages;
   s >> pages;

   size_t prev_pos = s.tellg();
   prev_pos++;
   size_t pos = buffer.find_first_of("[", prev_pos);
   std::string name = buffer.substr(prev_pos, pos - prev_pos);
   relations.insert({name, {segment_id, pages, name, {}}}); 
   pos++;
   prev_pos = pos;

   // read-in attributes
   RelationIU *riu = &relations.at(name);
   while ((pos = buffer.find_first_of("]", prev_pos)) != std::string::npos) {
      _deserializeAttribute(buffer.substr(prev_pos, pos - prev_pos), riu);
      pos++;
      prev_pos = pos + 1;
   } 
}

void Schema::_deserializeAttribute(const std::string &buffer, RelationIU *riu) {
   riu->attributes.push_back(AttributeIU());

   // read-in type
   size_t prev_pos = 1;
   size_t pos = buffer.find_first_of(")", prev_pos);
   _deserializeType(buffer.substr(prev_pos, pos - prev_pos), &riu->attributes.back());

   // read name
   prev_pos = pos + 1;
   riu->attributes.back().name = buffer.substr(prev_pos);
}

void Schema::_deserializeType(const std::string &buffer, AttributeIU *aiu) {
   std::istringstream s(buffer);

   // read type
   uint32_t type;
   s >> type;
   aiu->type.type = static_cast<TypeIU::Type>(type);
   char del;
   s >> del; 
   // read len
   s >> aiu->type.len;
   s >> del;
   // read dec
   s >> aiu->type.dec;
}


// SERIALIZATION HELPER
//--------------------------------------
std::ostream& operator<<(std::ostream& out, const Schema::RelationIU& r) {
   out << r.segment_id << SEPERATOR;
   out << r.pages << SEPERATOR;
   out << r.name;
   for (auto &a : r.attributes) {
      out << '[';
      out << a;
      out << ']';
   }
   out << '\0';
   return out;
}

std::ostream& operator<<(std::ostream& out, const Schema::AttributeIU& a) {
   out << '(';
   out << a.type;
   out << ')';
   out << a.name;
   return out;
}

std::ostream& operator<<(std::ostream& out, const Schema::TypeIU& t) {
   out << t.type << SEPERATOR;
   out << t.len << SEPERATOR;
   out << t.dec;
   return out;
}
