/*
 * BPTree.hpp
 *
 *  Created on: 20.05.2014
 *      Author: Daniel Hugenroth, Moritz Wilfer
 */

#ifndef BPTREE_HPP_
#define BPTREE_HPP_

#include <unordered_map>
#include <exception>
#include "../buffer/BufferManager.hpp"
#include "../buffer/BufferFrame.hpp"
#include "Schema.hpp"
#include "SPSegment.hpp"

// some custom exception
class key_not_found : public std::exception {
public:
  key_not_found() { }
  ~key_not_found() throw() { }
  const char* what() const throw() { return "key could not be found"; }
};


template <class T>
struct BTreeInnerPair {
    T key;
    page_id_t page_id;
};
template <class T>
struct BTreeLeafPair {
    T key;
    TID tid;
};

template <class T>
struct BTreeInnerNode {
    //LSN lsn;
    page_id_t upper;
    page_id_t count;
    BTreeInnerPair<T> entries[0];
};
template <class T>
struct BTreeLeafNode {
    //LSN lsn;
    page_id_t next;
    page_id_t count;
    BTreeLeafPair<T> entries[0];
};

template <class T, class CMP>
class BTree {
public:
    BTree(Schema::IndexIU *iiu, BufferManager *bm) : size_(0), iiu(iiu), bm(bm) { 
        max_inner_entries = (PAGE_SIZE - 1 - sizeof(BTreeInnerNode<T>)) / sizeof(std::pair<T, page_id_t>);
        max_leaf_entries = (PAGE_SIZE - 1 - sizeof(BTreeLeafNode<T>)) / sizeof(std::pair<T, TID>);
    }
    ~BTree() { }
    
    // inserts a new key,tid pair in the b+tree
    void insert(const T &key, const TID &tid) { 
    
        // get leaf page by tree traversal
        std::unordered_map<page_id_t, BufferFrame *> coupling;
        auto info = getLeafPage(key, coupling, true);
        BufferFrame &bf = *(info.first);
        BTreeLeafNode<T> &leaf = *reinterpret_cast<BTreeLeafNode<T> *>((char *)bf.getData() + 1);
        
        // check where to put entry
        if (leaf.count == 0) {
            leaf.entries[0] = {key, tid}; 
            leaf.count++;   
            bm->unfixPage(bf, true);
        } else {
            
            // check whether page need to be split
            if (spaceForEntry(bf)) {
            
                // move all entries from index by one - necessary to keep order
                size_t index = binarySearchForKeyLeaf(key, leaf);
                
                if (index == UINT64_MAX)
                    leaf.entries[leaf.count] = {key, tid};
                else {
                    for (size_t i = leaf.count; i > index; i--)
                        leaf.entries[i] = leaf.entries[i - 1];
                    leaf.entries[index] = {key, tid};
                }
                leaf.count++;
                bm->unfixPage(bf, true);
                
            } else {     
            
                size_t index = binarySearchForKeyLeaf(key, leaf);        
                         
                // allocate two new pages                    
                uint64_t pageId = ((uint64_t)iiu->segment_id << (sizeof(page_id_t) * 8)) + iiu->pages;
                iiu->incPages();                
                BufferFrame &bf_new = bm->fixPage(pageId, true);
                BTreeLeafNode<T> &leaf_new = *reinterpret_cast<BTreeLeafNode<T> *>((char *)bf_new.getData() + 1);
                
                // store information
                leaf_new.next = bf.getPageId();
                size_t count = leaf.count;
                leaf_new.count = (count + 1) / 2;
                leaf.count = count / 2;
                
                // copy data
                memcpy(&leaf_new.entries[0], &leaf.entries[0], leaf_new.count * sizeof(BTreeLeafPair<T>));
                for (int32_t i = leaf_new.count; i < count; i++)
                    leaf.entries[i - leaf_new.count] = leaf.entries[i];
                
                // copy new entry to proper place
                BufferFrame *place;
                if (index < leaf_new.count)
                    place = &bf_new;
                else
                    place = &bf;
                    
                BTreeLeafNode<T> &l = *reinterpret_cast<BTreeLeafNode<T> *>((char *)place->getData() + 1);
                // move all entries from index by one - necessary to keep order
                index = binarySearchForKeyLeaf(key, l);
                
                if (index == UINT64_MAX || index == l.count) {
                    l.entries[l.count] = {key, tid};
                } else {
                    for (size_t i = l.count; i > index; i--)
                        l.entries[i] = l.entries[i - 1];
                    l.entries[index] = {key, tid};
                }
                l.count++;
                
                // lock coupling -> free lock
                page_id_t sep_page = bf_new.getPageId();
                T sep_key = leaf_new.entries[leaf_new.count - 1].key;
                page_id_t child_r = bf.getPageId();
                bm->unfixPage(bf, true);
                bm->unfixPage(bf_new, true);
                
                // push maximum of left child as seperator to parent
                insertSeperator(info.second, sep_page, sep_key, child_r, coupling);
            }
            
        }
        
        size_++;        
    }
    
    // deletes a key,tid pair from the b+tree
    void erase(const T &key) {
        std::unordered_map<page_id_t, BufferFrame *> coupling;
        auto info = getLeafPage(key, coupling, true);
        
        BufferFrame &bf = *(info.first);
        BTreeLeafNode<T> &leaf = *reinterpret_cast<BTreeLeafNode<T> *>((char *)bf.getData() + 1);
        
        // do binary search for key on leaf page
        size_t index = binarySearchForKeyLeaf(key, leaf);
        BTreeLeafPair<T> entry = leaf.entries[index];
        
        // check whether key exists at all
        CMP c;
        if (c(entry.key, key) || c(key, entry.key)) {
            // free all latches
            bm->unfixPage(bf, true);
            freeCoupling(coupling);
            return;
        }    
                   
        // move entries
        if (leaf.count > 1)
            for (int32_t i = index; i < leaf.count; i++) 
                leaf.entries[i] = leaf.entries[i + 1];
        
        // free all latches
        bm->unfixPage(bf, true);
        freeCoupling(coupling);
        
        leaf.count--;
        size_--;     
    }
    
    bool lookup(const T &key, TID &tid) {
    
        // get leaf page by tree traversal
        std::unordered_map<page_id_t, BufferFrame *> coupling;
        auto info = getLeafPage(key, coupling);
        BufferFrame &bf = *(info.first);
        BTreeLeafNode<T> &leaf = *reinterpret_cast<BTreeLeafNode<T> *>((char *)bf.getData() + 1);
            
        // do binary search for key on leaf page
        size_t index = binarySearchForKeyLeaf(key, leaf);
        BTreeLeafPair<T> entry = leaf.entries[index];
        bm->unfixPage(bf, false);  
        
        // check whether we got right key
        CMP c;
        if (c(entry.key, key) || c(key, entry.key))
            return false;
        else {
            tid = entry.tid;
            return true;
        }
    }
    
    size_t size() const { return size_; }
private:
    size_t size_;
    page_id_t max_inner_entries;
    page_id_t max_leaf_entries;

    Schema::IndexIU *iiu;
    BufferManager *bm;  
    
    // implements lock coupling
    std::pair<BufferFrame *, std::vector<page_id_t>> getLeafPage(const T &key, std::unordered_map<page_id_t, BufferFrame *> &coupling, bool is_insert = false) {
        std::vector<page_id_t> path;
        
        page_id_t curr_page_id = iiu->root;
        uint64_t pageId = ((uint64_t)iiu->segment_id << (sizeof(page_id_t) * 8)) + curr_page_id;
        BufferFrame *bf = &bm->fixPage(pageId, is_insert);

        // search for leaf page
        while (1) {    
            if (!((char *) bf->getData())[0]) {
            
                // leaf node
                if (spaceForEntry(*bf))
                    freeCoupling(coupling);
                
                return {bf, path};
            
            } else {
            
                // inner node
                path.push_back(bf->getPageId());
                BTreeInnerNode<T> &inner = *reinterpret_cast<BTreeInnerNode<T> *>((char *) bf->getData() + 1);
                
                // search the next page
                page_id_t child = 0;
                size_t index = binarySearchForKeyInner(key, inner);
                BTreeInnerPair<T> entry = inner.entries[index];
                
                if (entry.page_id == PAGEID_MAX || index == inner.count)
                    child = inner.upper;
                else
                    child = entry.page_id;
                    
                // go to child
                pageId = ((uint64_t)iiu->segment_id << (sizeof(page_id_t) * 8)) + child;
                BufferFrame *bf2 = &bm->fixPage(pageId, is_insert); // root is always located at page 0
                
                // lock coupling
                if (!is_insert)
                    bm->unfixPage(*bf, false);
                else {
                    if (spaceForEntry(*bf2)) {
                        bm->unfixPage(*bf, false);
                        freeCoupling(coupling); // held latches above can be freed
                    } else
                        coupling[curr_page_id] = bf;
                }
                
                curr_page_id = child;
                bf = bf2;
            }
        }
    }
    
    // binary search for inner key (first >= key)
    // see http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=binarySearch
    size_t binarySearchForKeyInner(const T &key, const BTreeInnerNode<T> &inner) {
        
        /*if (inner.count == 0)
            throw key_not_found();*/
        
        CMP c; // comparer
        int32_t a = 0;
        int32_t b = inner.count - 1;
        int32_t middle;
        
        while (a < b) {
            middle = a + ((b - a + 1) / 2);
            
            if (c(key, inner.entries[middle].key)) 
                b = middle - 1;
            else
                if (a == middle)
                    a = middle + 1;
                else
                    a = middle;      
        }
        
        if (c(key, inner.entries[a].key))
            return 0;
            
        // check whether we found the key
        if (!c(key, inner.entries[a].key) && !c(inner.entries[a].key, key))
            return a;
            
        return a + 1; 
    }
    
    // binary search for leaf key (first >= key)
    // see http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=binarySearch
    size_t binarySearchForKeyLeaf(const T &key, const BTreeLeafNode<T> &leaf) {
        
        /*if (leaf.count == 0)
            throw key_not_found();*/
        
        CMP c; // comparer
        int32_t a = 0;
        int32_t b = leaf.count - 1;
        int32_t middle;
        
        while (a < b) {
            middle = a + ((b - a + 1) / 2);
            
            if (c(key, leaf.entries[middle].key)) 
                b = middle - 1;
            else
                a = middle;      
        }
        
        if (c(key, leaf.entries[a].key))
            return 0;
        
        // check whether we found the key
        if (!c(key, leaf.entries[a].key) && !c(leaf.entries[a].key, key))
            return a;
             
        return a + 1;
    }
    
    // checks whether there is space for another entry
    bool spaceForEntry(const BufferFrame &bf) {
        // check whether buffer frame is a leaf or inner node
        if (!((char *) bf.getData())[0]) {
            BTreeLeafNode<T> &leaf = *reinterpret_cast<BTreeLeafNode<T> *>((char *)bf.getData() + 1);
            return leaf.count < max_leaf_entries;
        } else {
            BTreeInnerNode<T> &inner = *reinterpret_cast<BTreeInnerNode<T> *>((char *)bf.getData() + 1);
            return inner.count < max_inner_entries;
        }
    }
    
    // inserts a new seperator key into an inner node
    void insertSeperator(std::vector<page_id_t> path, const page_id_t &child_l, const T &key, const page_id_t &child_r, std::unordered_map<page_id_t, BufferFrame *> &coupling) {
          
        if (path.size() == 0) {
        
            // build new root!
            uint64_t pageId = ((uint64_t)iiu->segment_id << (sizeof(page_id_t) * 8)) + iiu->pages;
            iiu->incPages();                
            BufferFrame &bf = bm->fixPage(pageId, true);
            
            // mark page as inner node
            reinterpret_cast<uint8_t *>(bf.getData())[0] = 1;
        
            // build root node
            BTreeInnerNode<T> &inner = *reinterpret_cast<BTreeInnerNode<T> *>((char *)bf.getData() + 1);
            inner.upper = child_r;
            inner.count = 1;
            inner.entries[0] = {key, child_l};
            
            iiu->root = bf.getPageId();
            
            // free latches
            bm->unfixPage(bf, true);
            freeCoupling(coupling);
        
        } else {
            
            page_id_t parent = path[path.size() - 1];
            BufferFrame &bf = *coupling[parent];
            BTreeInnerNode<T> &inner = *reinterpret_cast<BTreeInnerNode<T> *>((char *)bf.getData() + 1);
            
                
            if (spaceForEntry(bf)) {
                
                // move all entries from index by one - necessary to keep order
                size_t index = binarySearchForKeyInner(key, inner);
                
                if (index == UINT64_MAX || index == inner.count) {
                    inner.entries[inner.count] = {key, child_l};
                    inner.upper = child_r;
                } else {
                    for (size_t i = inner.count; i > index; i--)
                        inner.entries[i] = inner.entries[i - 1];
                    inner.entries[index] = {key, child_l};
                }
                inner.count++;
                
                // free all latches
                bm->unfixPage(bf, true); 
                coupling.erase(parent);
                freeCoupling (coupling);
                
            } else {
            
                size_t index = binarySearchForKeyInner(key, inner);
                
                // allocate two new pages                    
                uint64_t pageId = ((uint64_t)iiu->segment_id << (sizeof(page_id_t) * 8)) + iiu->pages;
                iiu->incPages();                
                BufferFrame &bf_new = bm->fixPage(pageId, true);
                
                // mark page as inner node
                reinterpret_cast<uint8_t *>(bf_new.getData())[0] = 1;
                
                BTreeInnerNode<T> &inner_new = *reinterpret_cast<BTreeInnerNode<T> *>((char *)bf_new.getData() + 1);
                
                // store information
                size_t count = inner.count;
                inner_new.count = (count + 1) / 2;
                inner.count = count / 2;
                
                // copy data
                memcpy(&inner_new.entries[0], &inner.entries[0], inner_new.count * sizeof(BTreeInnerPair<T>));
                for (int32_t i = inner_new.count; i < count; i++) 
                    inner.entries[i - inner_new.count] = inner.entries[i];
                
                // copy new entry to proper place
                BufferFrame *place;
                if (index < inner_new.count)
                    place = &bf_new;
                else
                    place = &bf;
                    
                BTreeInnerNode<T> &in = *reinterpret_cast<BTreeInnerNode<T> *>((char *)place->getData() + 1);
                // move all entries from index by one - necessary to keep order
                index = binarySearchForKeyInner(key, in);
                
                if (index == UINT64_MAX || index == in.count) {
                    in.entries[in.count] = {key, child_l};
                    in.upper = child_r;
                } else {
                    for (size_t i = in.count; i > index; i--)
                        in.entries[i] = in.entries[i - 1];
                    in.entries[index] = {key, child_l};
                }
                in.count++;
                
                // lock coupling -> free lock
                page_id_t sep_page = bf_new.getPageId();
                T sep_key = inner_new.entries[inner_new.count - 1].key;
                page_id_t child_r = bf.getPageId();
                bm->unfixPage(bf, true);
                bm->unfixPage(bf_new, true);
                coupling.erase(parent);
                
                // move seperator up
                path.pop_back();
                insertSeperator(path, sep_page, sep_key, child_r, coupling);
            }
            
        }
    }
    
    void freeCoupling(std::unordered_map<page_id_t, BufferFrame *> &coupling) {
        // unfix all pages
        for (auto &c : coupling)
            bm->unfixPage(*(c.second), false);
        coupling.clear();
    }   
};

#endif /* BPTREE_HPP_ */
