/*
 * SPSegment.hpp
 *
 *  Created on: 05.05.2014
 *      Author: Daniel Hugenroth, Moritz Wilfer
 */

#ifndef SPSEGMENT_HPP_
#define SPSEGMENT_HPP_

#include <cstdint>
#include <iostream>
#include <exception>
#include "../buffer/BufferManager.hpp"
#include "../buffer/BufferFrame.hpp"
#include "Schema.hpp"
#include "Record.hpp"

typedef uint64_t TID;
typedef uint64_t LSN;

// some custom exception
class free_slot : public std::exception {
public:
  free_slot() { }
  ~free_slot() throw() { }
  const char* what() const throw() { return "slot is free"; }
};
class indirection_error : public std::exception {
public:
  indirection_error(TID f_tid, TID s_tid) : f_tid(f_tid), s_tid(s_tid) { }
  ~indirection_error() throw() { }
  const char* what() const throw() {
    std::stringstream s;
    s << "Indirection error, searched for TID " << s_tid << " found " << f_tid;
    return s.str().c_str();
  }
private:
   TID f_tid;
   TID s_tid;
};

// implementation of slotted page (easy type-casting possible)
class SlottedPage {
public:
   // slot implementation
   struct Fields {
      uint8_t t;
      uint8_t s;
      uint16_t offset;
      uint16_t length;
   };

   union Slot {
      TID tid;
      Fields fields;
   };
   
   Slot &getSlot(const record_id_t &r_id);
   TID getTID(const uint16_t &offset);
   Record getRecord(const uint16_t &length, const uint16_t &offset);
   record_id_t insertRecord(const Record &r, bool indirected = false);
   void updateRecord(const record_id_t &r_id, const Record &r);
   bool hasFreeSpace(page_id_t size, bool compressed = false);
   void compress();

   LSN getLSN() const { return lsn; }
   void setLSN(LSN lsn_) { lsn = lsn_; }
   uint32_t getSlotCount() const { return slotCount; }
   uint32_t getFirstFreeSlot() const { return firstFreeSlot; }
   void incFreeSpaceBy(uint64_t size) { freeSpace += size; }
   void setFirstFreeSlot(record_id_t firstFreeSlot_) { firstFreeSlot = firstFreeSlot_; }
private:
   // header
   LSN lsn;
   record_id_t slotCount;
   record_id_t firstFreeSlot;
   uint64_t dataStart;
   uint64_t freeSpace;
   Slot slots[1];
};

// implementation of slotted page segment
class SPSegment {
public:
   SPSegment(Schema::RelationIU *riu, BufferManager *bm) : riu(riu), bm(bm) { }
   ~SPSegment() { }

   TID insert(const Record &r, bool indirected = false);
   bool remove(TID tid, bool indirected = false);
   Record lookup(TID tid, TID o_tid = 0);
   bool update(TID tid, const Record &r, bool indirected = false);
private:
   Schema::RelationIU *riu;
   BufferManager *bm;
};

#endif /* SPSEGMENT_HPP_ */
