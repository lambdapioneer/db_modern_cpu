// Test case for our schema implementation
#include <cstdlib>
#include <cstring>
#include "../buffer/BufferManager.hpp"
#include "../segment/Schema.hpp"

/*int main(int argc, char *argv[]) {  
   // create a schema
   std::cout << "[+] Create some relations" << std::endl;

   Schema *s = new Schema();
   s->addRelation("Studenten", {
                     {{Schema::TypeIU::Type::Int}, "MatrNr"},
                     {{Schema::TypeIU::Type::Varchar, 150}, "Name"},
                     {{Schema::TypeIU::Type::Tinyint}, "Semester"}   
                }); 
   s->addRelation("Vorlesungen", {
                     {{Schema::TypeIU::Type::Int}, "VorlNr"},
                     {{Schema::TypeIU::Type::Varchar, 150}, "Titel"},
                     {{Schema::TypeIU::Type::Tinyint}, "SWS"},
                     {{Schema::TypeIU::Type::Int}, "gelesenVon"}
                });

   const Schema::RelationIU *r1 = s->getRelationIU("Studenten");
   std::cout << "[ ] Name of Relation 1: " << r1->name << std::endl;
   std::cout << "[ ] Segment of Relation 1: " << r1->segment_id << std::endl;
   std::cout << "[ ] Pages of Relation 1: " << r1->pages << std::endl;
   std::cout << "[ ] # Attributes of Relation 1: " << r1->attributes.size() << std::endl << "-------------------------" << std::endl;

   const Schema::RelationIU *r2 = s->getRelationIU("Vorlesungen");
   std::cout << "[ ] Name of Relation 2: " << r2->name << std::endl;
   std::cout << "[ ] Segment of Relation 2: " << r2->segment_id << std::endl;
   std::cout << "[ ] Pages of Relation 2: " << r2->pages << std::endl;
   std::cout << "[ ] # Attributes of Relation 2: " << r2->attributes.size() << std::endl << "-------------------------" <<  std::endl << std::endl;

   // serialize schema 
   std::cout << "[+] Store schema to disk" << std::endl;
   std::string serialized = s->serialize();
   delete s;
   size_t s_size = serialized.size();
   std::cout << "[ ] Serialized-size: " << s_size << std::endl << std::endl;

   // assumption: whole schema fits in one page
   BufferManager *bm = new BufferManager(1000);
   BufferFrame& bf = bm->fixPage(0, true); // schema goes to page 0 of segment 0!
   memcpy((char *) bf.getData(), &s_size, sizeof(size_t));
   memcpy((char *) bf.getData() + sizeof(size_t), serialized.c_str(), serialized.size());
   bm->unfixPage(bf, true);

   // force write-out
   std::cout << "[+] Write out schema to disk" << std::endl;
   std::cout << "[ ] Serialized-size: " << serialized.size() << std::endl << std::endl;
   delete bm;
   
   // read-in schema again
   bm = new BufferManager(1000);   
   size_t s_size_n;
   std::cout << "[+] Read in schema from disk" << std::endl;
   BufferFrame& bf_l = bm->fixPage(0, true);
   memcpy(&s_size_n, (char *) bf_l.getData(), sizeof(size_t));
   std::cout << "[ ] Serialized-size: " << s_size_n << std::endl;
   std::string serialized_n((char *) bf_l.getData() + sizeof(size_t), s_size_n);
   s = new Schema(serialized_n);
   bm->unfixPage(bf_l, false);

   const Schema::RelationIU *r1_n = s->getRelationIU("Studenten");
   std::cout << "[ ] Name of Relation 1: " << r1_n->name << std::endl;
   std::cout << "[ ] Segment of Relation 1: " << r1_n->segment_id << std::endl;
   std::cout << "[ ] Pages of Relation 1: " << r1_n->pages << std::endl;
   std::cout << "[ ] # Attributes of Relation 1: " << r1_n->attributes.size() << std::endl << "-------------------------" << std::endl;

   const Schema::RelationIU *r2_n = s->getRelationIU("Vorlesungen");
   std::cout << "[ ] Name of Relation 2: " << r2_n->name << std::endl;
   std::cout << "[ ] Segment of Relation 2: " << r2_n->segment_id << std::endl;
   std::cout << "[ ] Pages of Relation 2: " << r2_n->pages << std::endl;
   std::cout << "[ ] # Attributes of Relation 2: " << r2_n->attributes.size() << std::endl << "-------------------------" <<  std::endl;

   delete bm;
}*/
