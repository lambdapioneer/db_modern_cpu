/*
 * BufferManager.cpp
 *
 *  Created on: 19.04.2014
 *      Author: Daniel Hugenroth, Moritz Wilfer
 */

#include "BufferManager.hpp"

BufferManager::~BufferManager() {
   for (auto& frame : fifo) {
      if (!frame)
         continue;
   
      // write back all dirty pages
      if (frame->isDirty()) {
         write_back(frame);
      }
      delete frame;
   }

   // deallocate all file descriptors
   for (auto &fd : page_tables) {
      //fflush(fd.second);   
      fclose(fd.second);
   }
   for (auto &fd : page_files) {
      //fflush(fd.second);   
      fclose(fd.second);
   }
}

BufferFrame& BufferManager::fixPage(uint64_t pageId, bool exclusive) {
   segment_id_t segment_id = pageId / PAGEID_MAX;
   page_id_t page_id = pageId; 
   
   single.lock();

   // check whether page is already in main memory
   if (pids[segment_id].find(page_id) == pids[segment_id].end()) {
      void *data = malloc(PAGE_SIZE);
      memset(data, 0, PAGE_SIZE);

      // check whether page can be read from disk
      if (curr_in_memory >= size) {
         // if not, try to free a page (may throw exception)  
         try_to_free_page();
      }

      // check whether page is available on disk
      int32_t page_position = -1;
      page_position = get_page_position(segment_id, page_id);

      if (page_position != -1) {

         // try to read page from file
         page_position = get_page_position(segment_id, page_id);

         if (page_position != -1) {
            // read page from file
            open_segment_file(segment_id);
   
            // seek to right position and read page
            uint32_t offset = page_position * PAGE_SIZE;
            fseek(page_files[segment_id], offset, SEEK_SET);
            if (fread(data, 1, PAGE_SIZE, page_files[segment_id]) != PAGE_SIZE)         
               exit(EXIT_FAILURE); // should not happen ;)

         }

      }
      curr_in_memory++;          
   
      // create new buffer frame
      BufferFrame *bf = new BufferFrame(segment_id, page_id, page_position, BufferFrame::pageState::CLEAN, data);
      
      // protect data structures
      pids[segment_id][page_id] = bf;
      fifo.push_back(bf);

   } else
      pids[segment_id][page_id]->fix();

   single.unlock();
      
   // try to acquire latch
   BufferFrame *bf = pids[segment_id][pageId];
   if (exclusive)
      bf->getExclLatch();
   else
      bf->getSharedLatch();  

   // return frame
   return *bf;
}

// unfixes a page (no need for instant write to disk!)
void BufferManager::unfixPage(BufferFrame& frame, bool isDirty) {
   if (isDirty)
      frame.makeDirty();
   // free latch
   frame.freeLatch();

   frame.unfix();
}

// read the page-table file to get available pages in file
uint32_t BufferManager::get_page_position(const segment_id_t segment_id, const page_id_t page_id) {
   uint32_t position = -1;
   
   if (pages_in_segment[segment_id].empty()) {
      open_segment_file(segment_id);
      
      rewind(page_tables[segment_id]);
      // get available pages
      pages_in_segment[segment_id].clear();
      page_positions[segment_id].clear();
      page_id_t pid;
      page_id_t i = 0;
      while (fread(&pid, 1, sizeof(page_id_t), page_tables[segment_id]) == sizeof(page_id_t)) {
         if (pid == page_id)
            position = i;

         // store in page-id vector
         pages_in_segment[segment_id].push_back(pid);
         page_positions[segment_id][pid] = i;
         i++;      
      } 
      pages_written_out[segment_id] = i;

   } else {
      // page table is in memory already
      if (page_positions[segment_id].find(page_id) != page_positions[segment_id].end())
         position = page_positions[segment_id][page_id];      
   }

   return position; 
}

// write page-table for a segment
void BufferManager::write_back_page_table(const segment_id_t segment_id) {
   open_segment_file(segment_id);
   
   // write pages (order is implicit)
   for (page_id_t i = pages_written_out[segment_id]; i < pages_in_segment[segment_id].size(); i++) 
      fwrite(&pages_in_segment[segment_id][i], sizeof(page_id_t), 1, page_tables[segment_id]);
   pages_written_out[segment_id] = pages_in_segment[segment_id].size();  
}

void BufferManager::write_back(BufferFrame *bf) {

   segment_id_t segment_id = bf->getSegmentId();
   page_id_t page_id = bf->getPageId();
   int32_t position = bf->getPosition();
   bool append = false; // indicates whether page is not yet available in file

   if (position == -1) {
      append = true;
      position = pages_in_segment[segment_id].size();
      pages_in_segment[segment_id].push_back(page_id); 
      page_positions[segment_id][page_id] = position;
      bf->setPosition(position); 
      
      // rewrite page-table
      write_back_page_table(segment_id);  
   }

   // check whether file has to be opened
   open_segment_file(segment_id);
   
   if (append) {
      // CASE 1: append    
      fseek(page_files[segment_id], 0L, SEEK_END);
   } else {
      // CASE 2: rewrite page, already available in file (we cannot store the entire file in main memory -> page per page copying)
      uint32_t offset = position * PAGE_SIZE;
      fseek(page_files[segment_id], offset, SEEK_SET);
   }
   fwrite(bf->getData(), 1, PAGE_SIZE, page_files[segment_id]);
}

// FIFO strategy for page replacement
void BufferManager::try_to_free_page() {
   BufferFrame *bf = nullptr;
 
   bool found = false;
   uint64_t i = 0;
   for (uint16_t j = 0; j < RETRIES; j++) {

      for (BufferFrame *f : fifo) {
         if (!f->isFixed()) {
            bf = f;
            found = true;
            break;   
         }
         i++;
      }
      // check whether we found a page
      if (found)
         break;

      usleep(1); // wait 10 milliseconds
   }

   // check whether a buffer frame can be freed
   if (bf == nullptr)
     throw unable_to_free();
   
   // write-back if page is dirty 
   if (bf->isDirty())
      write_back(bf);

   curr_in_memory--;

   // erase frame from fifo queue
   std::list<BufferFrame *>::iterator it = fifo.begin();
   std::advance(it, i);
   fifo.erase(it); // erase element from fifo "queue"
   pids[bf->getSegmentId()].erase(bf->getPageId()); 

   delete bf;   
}

void BufferManager::open_segment_file(const segment_id_t segment_id) {
   // open page table and page file itself
   if (page_tables.find(segment_id) == page_tables.end()) {
      page_tables[segment_id] = fopen(get_pt_filename(segment_id).c_str(), "a+b");
      
      page_files[segment_id] = fopen(get_filename(segment_id).c_str(), "r+b");      
      // check if file has to be created
      if (!page_files[segment_id])
         page_files[segment_id] = fopen(get_filename(segment_id).c_str(), "w+b");  
   }
}
