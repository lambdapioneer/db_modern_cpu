/*
 * BufferFrame.hpp
 *
 *  Created on: 19.04.2014
 *      Author: Daniel Hugenroth, Moritz Wilfer
 */

#ifndef BUFFERFRAME_HPP_
#define BUFFERFRAME_HPP_

#include <cstdlib>
#include <cstdint>
#include <iostream>
#include <atomic>
#include <mutex>

typedef uint32_t segment_id_t;
typedef uint32_t page_id_t;
#define PAGEID_MAX UINT32_MAX

// some custom exception
class unable_to_get_latch : public std::exception {
public:
  unable_to_get_latch() { }
  ~unable_to_get_latch() throw() { }
  const char* what() const throw() { return "unable to get latch"; }
};

class unable_to_free_latch : public std::exception {
public:
  unable_to_free_latch() { }
  ~unable_to_free_latch() throw() { }
  const char* what() const throw() { return "unable to free latch"; }
};


class BufferFrame {
public:
   // page states
   enum pageState { CLEAN, DIRTY };
   
   BufferFrame(segment_id_t segment_id, page_id_t page_id, int32_t position, pageState state, void *data) 
            : fixed(1), segment_id(segment_id), page_id(page_id), position(position), state(state), data(data) { 
      pthread_rwlock_init(&latch, NULL);
   }
   ~BufferFrame() {
      pthread_rwlock_destroy(&latch);
      free(data);
   }

   // some simple getter methods
   segment_id_t getSegmentId() const { return segment_id; }
   page_id_t getPageId() const { return page_id; }
   int32_t getPosition() const { return position; }
   void setPosition(int32_t pos) { position = pos; }
   void *getData() const { return data; }

   // methods to fix / unfix page
   void fix() { fixed++; }
   void unfix() { fixed--; }
   bool isFixed() const { return fixed != 0; }

   // methods to indicate whether page is dirty or not
   void makeDirty() { state = pageState::DIRTY; }
   bool isDirty() const { return state == pageState::DIRTY; }

   // get shared latch
   void getSharedLatch() {
      if (pthread_rwlock_rdlock(&latch) != 0)
         throw unable_to_get_latch(); 
   }  
   // get exclusive latch
   void getExclLatch() {
      if (pthread_rwlock_wrlock(&latch) != 0)
         throw unable_to_get_latch();   
   }   
   // free latch
   void freeLatch() {
      if (pthread_rwlock_unlock(&latch))
         throw unable_to_free_latch();
   }

private:
   std::mutex single;
   std::atomic<uint32_t> fixed;

   segment_id_t segment_id;
   page_id_t page_id;
   int32_t position;
   pageState state;
   void *data;

   // latch
   pthread_rwlock_t latch;
};

#endif /* BUFFERFRAME_HPP_ */
